FROM gcc:7

LABEL name="LALSuite Development - GCC 7" \
      maintainer="Adam Mercer <adam.mercer@ligo.org>" \
      support="Unsupported"

# ensure non-interactive debian installation
ENV DEBIAN_FRONTEND noninteractive
RUN echo 'debconf debconf/frontend select Noninteractive' | debconf-set-selections

# support https repositories
RUN apt-get update && \
    apt-get --assume-yes install \
      apt-transport-https \
      apt-utils \
      bash-completion \
      curl \
      lsb-release \
      wget

# Add other repos
RUN wget http://software.ligo.org/lscsoft/debian/pool/contrib/l/lscsoft-archive-keyring/lscsoft-archive-keyring_2016.06.20-2_all.deb && \
    apt --assume-yes install ./lscsoft-archive-keyring_2016.06.20-2_all.deb && \
    rm -f lscsoft-archive-keyring_2016.06.20-2_all.deb

RUN echo "deb http://software.ligo.org/gridtools/debian buster main" > /etc/apt/sources.list.d/gridtools.list && \
    echo "deb [trusted=yes] https://galahad.aei.mpg.de/lsc-amd64-buster ./" > /etc/apt/sources.list.d/lscsoft.list

# install dependencies
# NOTE: can't use lscsoft-lalsuite-dev as this brings in a dependency on
# liboctave-dev and libopenmpi-dev which is not compatible with the
# gfortran supplied by the gcc docker image
RUN apt-get update && apt-get --assume-yes install \
        autoconf \
        automake \
        bc \
        build-essential \
        ccache \
        doxygen \
        git-lfs \
        help2man \
        ldas-tools-framecpp-c-dev \
        libcfitsio-dev \
        libchealpix-dev \
        libfftw3-dev \
        libframe-dev \
        libglib2.0-dev \
        libgsl0-dev \
        libhdf5-dev \
        libmetaio-dev \
        libtool \
        libxml2-dev \
        pkg-config \
        python-all-dev \
        python-dateutil \
        python-dev \
        python-freezegun \
        python-glue \
        python-gwdatafind \
        python-gwpy \
        python-h5py \
        python-healpy \
        python-ligo-gracedb \
        python-ligo-lw \
        python-ligo-segments \
        python-matplotlib \
        python-mock \
        python-numpy \
        python-pytest \
        python-scipy \
        python-shapely \
        python-six \
        python3-all-dev \
        python3-dateutil \
        python3-dev \
        python3-freezegun \
        python3-glue \
        python3-gwdatafind \
        python3-gwpy \
        python3-healpy \
        python3-ligo-gracedb \
        python3-ligo-lw \
        python3-ligo-segments \
        python3-matplotlib \
        python3-numpy \
        python3-pytest \
        python3-scipy \
        python3-shapely \
        python3-six \
        swig3.0 \
        texlive-binaries

# git-lfs post-install
RUN git lfs install

# clear package cache
RUN rm -rf /var/lib/apt/lists/*